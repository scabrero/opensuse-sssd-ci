# OpenSUSE based images for sssd CI

This project builds OpenSUSE based docker images suitable to run the sssd
test suite. They are also useful for development or debugging.
